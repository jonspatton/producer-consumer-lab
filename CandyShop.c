#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <semaphore.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
 
int Ticket = 0;
 
sem_t *servers;
sem_t *customers;
sem_t *mutex;

void *customer()
{
  sem_wait(mutex);
  Ticket++;
  sem_post(mutex);
  printf("customer with ID %u arrived in the store\n", (int) pthread_self());
  sem_post(customers);
  sem_wait(servers);
  sleep(rand()%10); // buying candy	
  printf("customer with ID %u has been served (ticket: %i)\n", (int) pthread_self(), Ticket);
  pthread_exit(NULL);

  return (void*) 0;
}

void *server()
{
    int myid = (int) pthread_self();
    for(; ;)
    {
        sem_wait(customers);
        printf("Server with TID= %u is serving a customer\n", myid); 
        sleep(rand()%36); // serving the customer
        printf("Server with TID= %u is done\n", myid);
        sem_wait(mutex);
        Ticket --;
        sem_post(mutex);
        sem_post(servers);
    }
    return (void*) 0;
}

int main()
{
	int i;
	//sem_init(&servers, 0, 3);
	//sem_init(&customers, 0, 0);
	//sem_init(&mutex, 0,1);

  sem_unlink("servers");
  sem_unlink("customers");
  sem_unlink("mutex");

  mutex=sem_open("mutex", O_CREAT, 0644, 1);
	customers=sem_open("customers", O_CREAT, 0644, 0);
  servers=sem_open("servers", O_CREAT, 0644, 3);
	
	pthread_t sid[3], cid[50];
	srand(time(NULL));
	for(i=0; i<3; i++)
		pthread_create(&sid[i], NULL, server, NULL);
	for(i=0; i<50; i++)
	{
	 	pthread_create(&cid[i], NULL, customer, NULL);
		sleep(rand()%8);
	}
	
	for(i=0; i<3; i++)
		pthread_join(cid[i], NULL);
  
  sem_close(servers);
  sem_close(customers);
  sem_close(mutex);

  sem_unlink("servers");
  sem_unlink("customers");
  sem_unlink("mutex");
}
