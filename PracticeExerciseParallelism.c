#include <pthread.h>
#include <stdio.h>
#include <time.h>
#include <semaphore.h>
 
//semaphore used for mutual exclusion 
sem_t mutex; 

void *do_something_x( )
{ 
	sleep(rand() % 10);
	 
	printf(" In Thread X -- Entering CS  \n");
	sleep(rand() % 10);  // accessing & modifying Shared Data
	printf(" In Thread X -- Leaving CS  \n");

	return NULL;
}
void *do_something_y( )
{
	sleep(rand() % 10);
	 
	printf(" In Thread Y -- Entering CS  \n");
	sleep(rand() % 10);  // accessing & modifying Shared Data
	printf(" In Thread Y -- Leaving CS  \n");
	 
	return NULL;
}

void *do_something_z( )
{
	sleep(rand() % 10);
	 
	printf(" In Thread Z -- Entering CS  \n");
	sleep(rand() % 10);  // accessing & modifying Shared Data
	printf(" In Thread Z -- Leaving CS \n");
	 
	return NULL;
}
int main()
{
	 
	int i;
	pthread_t tidx, tidy, tidz;
	 
	srand(time(NULL));
	 
	sem_init(&mutex, 0, 1);
	 
	if(pthread_create(&tidx, NULL,do_something_x  , NULL)){
	 	printf("Error creating thread X\n");
		return 1;
	}
	if(pthread_create(&tidy, NULL,do_something_y  , NULL)){
	 	printf("Error creating thread Y\n");
		return 1;
	}
	
	if(pthread_create(&tidz, NULL,do_something_z  , NULL)){
	 	printf("Error creating thread Z\n");
		return 1;
	}
	
	 
	/* wait for the threads to finish */
	 
	if(pthread_join(tidx, NULL)) {
	printf("Error joining thread\n");
		return 2;
			}
	if(pthread_join(tidy, NULL)) {
	printf("Error joining thread\n");
		return 2;
			}
	if(pthread_join(tidz, NULL)) {
	printf("Error joining thread\n");
		return 2;
			}
	 
	return 0;
}
