#include <stdio.h>
#include <sys/types.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/shm.h>
#include "shared_lab6.h"

void Put()
{
	char item = 'A' + s->nextIn %26;
  	s->buffer[s->nextIn] = item;
  	s->nextIn = (s->nextIn + 1) % BUFF_SIZE;
	printf("PID %i producing %c ...\n", getpid(), item);
}

void * Producer()
{
    int i;

    for(i = 0; i < 10; i++)
    {
		sem_wait(empty_slots);
  		sem_wait(mutex);
      	Put();
		sem_post(mutex);
  		sem_post(full_slots_unprocessed);
    }
	return (void *) 0;
}

int main()
{

	mutex=sem_open(sem_name1, O_CREAT | O_EXCL, S_IRWXU, 0644, 1);
	empty_slots=sem_open(sem_name2, O_CREAT | O_EXCL, S_IRWXU, 0644, 1);
	//full_slots_processed=sem_open(sem_name3, O_CREAT | O_EXCL, S_IRWXU, 0644, 0);
    full_slots_unprocessed=sem_open(sem_name4, O_CREAT | O_EXCL, S_IRWXU, 0644, 0);

	//allocate the shared memory segment
	key_t key;
	key = 1234;
	//create the segment
	int shmid;
	
	if ((shmid = shmget(key, sizeof(shared_data), IPC_CREAT |0666)) <0)
	{
		perror("Shmget");
		exit(1);
	}
	//attach to the segment
	if ((shm = (shared_data *) shmat(shmid, NULL, 0))==(shared_data *) -1)
	{
		perror("Shmat");
		exit(1);
	}
	
	s=shm;
	s->nextIn = 0;
	 	
	Producer( );
        //detach
	shmdt((void *) shm);

    //sem_close(mutex);
    //sem_close(empty_slots);
    //sem_close(full_slots_unprocessed);
    
	return 0;
}