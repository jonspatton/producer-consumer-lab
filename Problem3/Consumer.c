#include <stdio.h>
#include <sys/types.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/shm.h>
#include "shared_lab6.h"

void Get()
{
  char item = s->buffer[s->nextOut]; 
  s->nextOut = (s->nextOut + 1) % BUFF_SIZE;
  printf("PID %i consuming %c ...\n", getpid(), item);
}

void * Consumer()
{
    int i;
    //char  item;
    for(i = 0; i < 10; i++)
    {
		sem_wait(full_slots_processed);
  		sem_wait(mutex);
      	Get();
		sem_post(mutex);
  		sem_post(empty_slots);
    }
	return (void *) 0;
}

int main()
{
	mutex=sem_open(sem_name1, O_CREAT | O_EXCL, S_IRWXU, 0644, 1);
	empty_slots=sem_open(sem_name2, O_CREAT | O_EXCL, S_IRWXU, 0644, 1);
	full_slots_processed=sem_open(sem_name3, O_CREAT | O_EXCL, S_IRWXU, 0644, 0);
    //full_slots_unprocessed=sem_open(sem_name4, O_CREAT | O_EXCL, S_IRWXU, 0644, 0);

	//allocate the shared memory segment
	key_t key;
	key = 1234;
	//locate the segment
	int shmid;
	if ((shmid = shmget(key, sizeof(shared_data),0666)) <0)
	{
		perror("Shmget");
		exit(1);
	}
	//attach to the segment
	if ((shm = (shared_data *) shmat(shmid, NULL, 0))==(shared_data *) -1)
	{
		perror("Shmat");
		exit(1);
	}
	
	s=shm;
	s->nextOut = 0;	
	Consumer();
	shmdt((void *) shm);

	//sem_close(mutex);
    //sem_close(empty_slots);
    //sem_close(full_slots_processed);
    

	return 0;
}
