
#define BUFF_SIZE 20

typedef struct {
char buffer[BUFF_SIZE];
int nextIn;
int nextOut;
int nextToProcess;
} shared_data;

shared_data *shm, *s;
char sem_name1[] = "mutex";
char sem_name2[] = "empty_slots";
char sem_name3[] = "full_slots_unprocessed";
char sem_name4[] = "full_slots_processed";

sem_t *empty_slots;
sem_t *full_slots_unprocessed;
sem_t *full_slots_processed;
sem_t *mutex;

shared_data *shm, *s;

