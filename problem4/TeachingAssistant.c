#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <semaphore.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
 
int Waiting = 0;
 
sem_t *ta;
sem_t *students;
sem_t *mutex;

void *student()
{
  sem_wait(mutex);
  //If all the chairs are full, go back to programming
  if (Waiting >= 3)
  {
      printf("Student %u is programming since the chairs are full.\n", (int) pthread_self());
      sem_post(mutex);
      sleep(rand()%30+10);
      student();
  }
  //If there's a free chair, add to the waiting queue, and let the TA know there are students.
  else{
    Waiting++;
    sem_post(mutex);
    sem_post(students);
    //If the TA isn't busy, we can get help from them.
    if (sem_wait(ta) <= 0)
    {
        printf("Student with ID %u is getting help from the TA\n", (int) pthread_self());
        // Getting help -- fudging a bit on the timing here, to match the TA's.
        //It won't be exact but it'll be close enough.
        sleep(3);
        printf("Student with ID %u got help and is back to programming\n", (int) pthread_self());
    }
    //Go back to programming for a while.
    sleep(rand()%30 + 10);
    //Recurse. (Or could have used an infinite loop.)
    student();
  }
  return (void*) 0;
}

void *teachingAssistant()
{
    for(; ;)
    {
        //Check if anyone's outside the door.
        sem_wait(mutex);
        if (Waiting <= 0)
        {
            //If they aren't, take a nap.
            printf("TA is sleeping ...\n");
            sem_post(mutex);
            //The sleep(1) call here is to slow the code down when they go back to sleep while
            //the students are programming. Otherwise it fills the entire screen several times
            //before one of the random sleeps finishes and someone wakes them up.
            sleep(1);
        }
        else {
            //Bring in one of the students.
            sem_wait(students);
            printf("TA is helping a student\n");
            Waiting --;
            sem_post(mutex);
            //Help them for a little bit of time
            //I fixed this at 3 because otherwise the TA would never get back to sleep.
            sleep(3);
            printf("TA is finished with student.\n");
            //TA is available again.
            sem_post(ta);
        }
    }
    return (void*) 0;
}

int main()
{
	int i;

    //Delete the semaphores in case they were created by a previous run.
    //The code as written just runs forever so we need to make sure these
    //aren't already hanging around with some old (bad) state.
    sem_unlink("ta");
    sem_unlink("students");
    sem_unlink("mutex");
    mutex=sem_open("mutex", O_CREAT, 0644, 1);
    students=sem_open("students", O_CREAT, 0644, 0);
    ta=sem_open("ta", O_CREAT, 0644, 1);
	
	pthread_t taid, cid[10];
	srand(time(NULL));
	pthread_create(&taid, NULL, teachingAssistant, NULL);

    //Create the TA
	for(i=0; i<3; i++)
    {
		pthread_join(cid[i], NULL);
    }   

    //Create 10 students
	for(i=0; i<10; i++)
	{
	 	pthread_create(&cid[i], NULL, student, NULL);
	}
	
  //Note that the code as written never reaches these.
  sem_close(ta);
  sem_close(students);
  sem_close(mutex);

  sem_unlink("ta");
  sem_unlink("students");
  sem_unlink("mutex");
}
