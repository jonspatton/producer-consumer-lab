#include <stdio.h>
#include <sys/types.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/shm.h>

#define BUFF_SIZE 20


 
char sem_name1[] = "mutex";
sem_t *mutex;
int *s;

void INC( )
{
  int i;
  for(i=1;i<10;i++)
  {
   sem_wait(mutex);
    (*s)++;
    printf("Increment by Process %d %d\n", getpid(), *s);
   sem_post(mutex);
   sleep(rand()%5);
  }
}

void main()
{
	
	mutex=sem_open(sem_name1, O_CREAT,0644, 1);
	 
	//allocate the shared memory segment
	key_t key;
	key = 2345;
	//create the segment
	int shmid;
	
	if ((shmid = shmget(key, sizeof(int), IPC_CREAT |0666)) <0)
	{
		printf("Shmget\n");
		exit(1);
	}
	//attach to the segment
	if ((s= (int *) shmat(shmid, NULL, 0))==(int *) -1)
	{
		printf("Shmat\n");
		exit(1);
	}
	
	*s = 0;
	 	
	INC();
	printf("Final result = %d printed by %d\n", *s,getpid());
	 
        //detach
	shmdt((void *) s);
}
