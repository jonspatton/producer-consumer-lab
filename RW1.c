#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <semaphore.h>

#define N 20

void *reader(void *vargp);
void *writer(void *vargp);

 
int rcount;
sem_t wsem, mutex;  

int main()
{
        int i;
	rcount=0;
        pthread_t tid[N];
	 
	sem_init(&mutex, 0,1);
	sem_init(&wsem, 0,1);
        srand(time(NULL));
	 
        for (i = 0; i < N; i++)
	{
		if(rand()%2)
                	pthread_create(&tid[i], NULL, reader, (void *)i);
		else
                	pthread_create(&tid[i], NULL, writer, (void *)i);
	}
         
 	for (i = 0; i < N; i++)
		pthread_join(tid[i], NULL);
	
	printf("Good BYE\n");
}

void *reader(void *vargp)
{	
	 
	int myid= (int) vargp;
	//sleep(rand()%8);
	sem_wait(&mutex);
		rcount++;
		if(rcount == 1)
			sem_wait(&wsem);
	sem_post(&mutex);
	printf("R [%d] is IN\n", myid);
	sleep(rand()%4);
	printf("R [%d] is OUT\n", myid);
	sem_wait(&mutex);
		rcount--;
		if(rcount == 0)
			sem_post(&wsem);
	sem_post(&mutex);	 
 }
void *writer(void *vargp)
{	
	 
	int myid= (int) vargp;
	 
	sleep(rand()%8);
	sem_wait(&wsem);	 
		printf("W [%d] is IN\n", myid);
		sleep(rand()%4);
 		printf("W [%d] is OUT\n", myid);
	sem_post(&wsem);	 
 }

