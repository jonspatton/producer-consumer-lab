#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdlib.h>

#define BUFF_SIZE 20

char buffer[BUFF_SIZE];
int nextIn = 0;
int nextOut = 0;

//Sync semaphore blocking producer
sem_t *empty_slot;

//sync semaphore blocking consumer
sem_t *full_slot;

//mutual exclusion
sem_t *mutex;

int size(char buff[])
{
    int i;
    for (i = 0; i<BUFF_SIZE; i++)
    {
        if (buffer[i] == 0) return i;
    }
    return i;
}

//Producer checks if there's an empty buffer; if there is, mutually excludes, adds to the buffer, releases locks (signal)
void Put(char item)
{
    buffer[nextIn] = item;
    nextIn = (nextIn + 1) % BUFF_SIZE;
    printf("Producing %c ...\n", item);
}

//Producer checks if there's a full buffer; if there is, mutually excludes, removes to the buffer, releases locks (signal)
void Get(char item)
{
    item = buffer[nextOut];
    nextOut = (nextOut + 1) % BUFF_SIZE;
    printf("Consuming %c ...\n", item);
}

void * Producer() 
{
    int i;
    for (i = 0; i < 15; i++)
    {
        sem_wait(empty_slot);
        sem_wait(mutex);
        Put('A' + (i % 26));
        sem_post(mutex);
        sem_post(full_slot);
    }
    return (void*) 0;
}

void * Consumer()
{
    int i;
    for (i = 0; i < 15; i++)
    {
        sem_wait(full_slot);
        sem_wait(mutex);
        Get('A' + (i % 26));
        sem_post(mutex);
        sem_post(empty_slot);
    }
    return (void*) 0;
}

int main()
{
    sem_unlink("empty_slot");
    sem_unlink("full_slot");
    sem_unlink("mutex");
    //Initialize semaphores

    //This one has to be initialized to 1 -- there's nothing there yet.
    //Mode explanation: https://stackoverflow.com/questions/18415904/what-does-mode-t-0644-mean
    if ((empty_slot = sem_open("empty_slot", O_CREAT, S_IRWXU, 0777, 1)) == SEM_FAILED)
    {
        sem_unlink("empty_slot");
        printf("empty_slot failed\n");
        return 0;
    };
    full_slot = sem_open("full_slot", O_CREAT, S_IRWXU, 0777, 0);
    mutex = sem_open("mutex", O_CREAT, S_IRWXU, 0777, 0);

    pthread_t pid, cid;
    pthread_create(&pid, NULL, Producer, NULL);
    pthread_create(&cid, NULL, Consumer, NULL);
    
    pthread_join(pid, NULL);
    pthread_join(cid, NULL);

    sem_unlink("empty_slot");
    sem_unlink("full_slot");
    sem_unlink("mutex");
}